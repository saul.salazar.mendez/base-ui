
var vm = new Vue({
    el: "#main",
    components: {
        'formulario': formulario
    },
    methods: {
        print(data) {
            console.log(data);
        }
    },
    template: `
    <div>
    <h1>Hola mundo</h1>
    <formulario @submit="print($event)"/>
    </div>
    ` 
  });