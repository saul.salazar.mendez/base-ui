Vue.component('lista-componentes',{
    props: ['ideData', 'creaCopia', 'move'],
    methods: {
    },
    template: `
    <div>
        <div class="row" v-for="(componentes, index1) in ideData.arregloComponentes.items">
            <div class="col-md-12" style="padding: 10px">
                <button type="button" class="btn btn-primary" data-toggle="collapse" :data-target="'#form-'+componentes.name">
                    {{componentes.titulo}}
                </button>
            </div>
            <div :id="'form-'+componentes.name" class="col-md-12 collapse">
            <draggable v-model="ideData.arregloComponentes.items[index1].items" class="dragArea" 
            :options="{ group:{ name: componentes.grupo,  pull:'clone', put:false }}" 
            :clone="creaCopia"
            :move="move">
                <span class="badge badge-primary" v-for="(element, index) in ideData.arregloComponentes.items[index1].items" :key="index" >{{element.label.value}}</span>
            </draggable>
            </div>
        </div>
    </div>
    `
});

Vue.component('ide', {
    props: ['ideData'],
    data: ()=>{
        return {
            selectItem: null,
            indexSelected: null,
            //contenedorSelected: null,
            valid: true,
            eventSelect: null,
            eventSubContenedorSelect: null,
            contVariable: new Foliadora(false)
        }
    },
    methods: {
        setSelected(data) {
            this.selectItem = null;
            this.indexSelected = null;
            setTimeout(() => {
                this.selectItem = data.item;
                this.indexSelected = data.index;
                this.contenedorSelected = data.contenedor;
            }, 100);
        },
        selectContenedor(data){
            this.eventSelect = null;
            setTimeout(() => {
                this.eventSelect = data;
            }, 100);
        },
        selectSubContenedor(data){
            this.eventSubContenedorSelect = null;
            setTimeout(() => {
                this.eventSubContenedorSelect = data;
            }, 100);
        },
        guardaComponenteHtml() {
            this.ideData.guardaComponenteHtml();
        },
        guardaComponenteVue() {
            this.ideData.guardaComponenteVue();
        },
        guardaComponenteAngular() {
            this.ideData.guardaComponenteAngular();
        },
        creaCopia(item) {
            return this.ideData.creaCopia(item);
        },
        move(event, old) {
            return this.ideData.move(event,old);
        },
        getDataForm() {
            let data = {};
            for (const item of this.formulario) {
                if (item instanceof BaseInput) { 
                    data[item.name.value] = null;
                }
            }
            return data;
        },
        /**
         * Obtiene el modelo del contenedor
         * @param {*} contenedor 
         * @param {*} data 
         */
        getModelFromContenedor(contenedor, model) {
            for (const item of contenedor.items) {
                if (item instanceof BaseInput) { 
                    model[item.name.value] = null;
                }
            }
        },
        /**
         * Obtiene el modelo de datos del arreglo de contenedores.
         * @param {*} array 
         * @param {*} model 
         */
        getModelFromArrayContenoders(array, model) {
            for (const contenedor of array.items) {
                if (contenedor instanceof PanelModel || contenedor instanceof AcordeonModel) {
                    this.getModelFromContenedor(contenedor, model);
                }
            }
        },
        /**
         * regresa el modelo de datos del formulario
         */
        getModelForm() {
            let model = {};
            for (const contenedor of this.ideData.formulario.items) {
                if (contenedor instanceof PanelModel || contenedor instanceof AcordeonModel) {
                    this.getModelFromContenedor(contenedor, model);
                }
            }
            return model;
        },
        save(event){
            event.preventDefault();
            const form = event.target;
            let data = this.getModelForm();
            for (const key in data) {
                const input = form.querySelector('input[name="'+key+'"]');
                if (input) {
                    data[key] = input.value;
                    if (input.type == 'checkbox') {data[key] = input.checked;}
                }else if (form.querySelector('textarea[name="'+key+'"]')){
                    data[key] = form.querySelector('textarea[name="'+key+'"]').value;
                }
            }
            confirm('Datos: '+ JSON.stringify(data, null, '\t') );
        },
        formValid(){
            const form = document.querySelector('form');
            this.valid = form.checkValidity();
        },
        eliminarItem(index) {
            this.contenedorSelected.items.splice(index,1);
            this.clearSelect();
        },
        clearSelect() {
            this.selectItem = null;
            this.indexSelected = null;
            this.contenedorSelected = null;
            this.eventSelect = null;
            this.eventSubContenedorSelect= null;
        },
        eliminarContenedor(event) {
            if (!event) {return;}
            let padre = event.padre;
            padre.items.splice(event.index, 1);
            this.clearSelect();
        },
        imprime(item) {   
            console.log(item);
        }
    },
    mounted: function (){
        const boxes = (function() {
            let boxesArray = document.getElementsByClassName('content-box');
            let boxes = {}, i = 1;
            for (box of boxesArray) {
              boxes['box' + i++] = box;
            }
            return boxes;
        }());
          
        for (resizer of document.getElementsByClassName('resizer')) {
            resizer.addEventListener('mousedown', e => {
                e.preventDefault();
                let resizer = e.target
                if (resizer.classList.contains('vertical')) {
                    document.body.style.cursor = 'e-resize';
                } else {
                    document.body.style.cursor = 'n-resize';
                }
            
                document.addEventListener('mousemove', resize);
                document.addEventListener('mouseup', e => {
                    document.removeEventListener('mousemove', resize);
                    document.body.style.cursor = 'auto';
                });
            
                function resize(e) {
                    if (resizer.classList.contains('de-componente')) {
                        return resizeX(e)
                    }
                    return resizeX2(e);
                }
                function resizeX(e) {
                    console.log('X1');
                    let bodyWidth = 100 / document.body.clientWidth;
                    let leftHeight = (parseFloat(getComputedStyle(boxes.box1, '').width) + e.movementX) * bodyWidth;
                    let rightHeight = (parseFloat(getComputedStyle(boxes.box3, '').width)) * bodyWidth;
                    boxes.box1.style.width =  leftHeight + '%';
                    boxes.box2.style.width =  (100 - leftHeight - rightHeight) + '%';
                    boxes.box2.style.left =  leftHeight + '%';
                    boxes.box2.style.right = rightHeight + '%';
                }
                function resizeX2(e) {
                    console.log('X2');
                    let bodyWidth = 100 / document.body.clientWidth;
                    let rightHeight = (parseFloat(getComputedStyle(boxes.box3, '').width) - e.movementX) * bodyWidth;
                    let leftHeight = (parseFloat(getComputedStyle(boxes.box1, '').width)) * bodyWidth; 
                    boxes.box3.style.width =  rightHeight + '%';
                    boxes.box2.style.width =  (100 - rightHeight - leftHeight) + '%';
                    boxes.box2.style.right =  rightHeight + '%';
                    boxes.box2.style.left = leftHeight + '%';
                }
                function resizeY(e) {
                    let bodyHeight = 100 / document.body.clientHeight;
                    let topHeight = (parseFloat(getComputedStyle(boxes.box2, '').height) + e.movementY) * bodyHeight;
                    boxes.box2.style.height = topHeight + '%';
                }
            });
        }
    },
    template:`
    <div>
        <div class="content-box panel-componenetes top left no-scroll">
        <div class="header">
            Componentes
        </div>
        <div>
            <div class="big-space" style="width: 90%; padding:10px; border: dotted navy 1px; margin-left: 10px;">
                Arrastrar y soltar
            </div>

            <lista-componentes :ide-data="ideData" 
            :crea-copia="creaCopia.bind(this)"
            :move="move.bind(this)"></lista-componentes>
        </div>
        <div class="resizer vertical de-componente"></div>
        </div>


        <div class="content-box panel-editor top centrado">
            <div class="header">
                Formulario
            </div>
            <div style="width: 90%; padding-left: 50px;">
                <div >
                    <h1 class="derecha">
                        <button class="btn btn-primary" @click="guardaComponenteHtml()">
                            Html <sub><i class="glyphicon glyphicon-save"></i></sub>
                        </button>

                        <button class="btn btn-primary" @click="guardaComponenteVue()">
                            Vue <sub><i class="glyphicon glyphicon-save"></i></sub>
                        </button>

                        <button class="btn btn-primary" @click="guardaComponenteAngular()">
                            <img src="./img/angular.ico" style="width: 20px"> <sub><i class="glyphicon glyphicon-save"></i></sub>
                        </button>
                    </h1>
                    <form v-on:submit="save($event)" @change="formValid()">
                    <draggable v-model="ideData.formulario.items" class="back-white" class="back-white" :options="{group:'contenedores'}">
                    <contenedor-constructor v-for="(contenedor,index1) in ideData.formulario.items" 
                        :contenedor="ideData.formulario.items[index1]"
                        :index="index1"
                        :padre="ideData.formulario"
                        :valid ="valid"
                        @selectItem="setSelected($event)" 
                        @selectContenedor="selectContenedor($event)"
                        @selectContenedorTab="selectSubContenedor($event)"></contenedor-constructor>
                    
                    </draggable>
                    </form>
                </div>
            </div>
            <div class="resizer vertical de-propiedad"></div>
        </div>

        <div class="content-box panel-propiedades top right">
            <div class="header">
                Propiedades
            </div>
            <div class="big-space"></div>
            <div style="width: 80%;">
                <div class="row">
                    <propiedades-de-contenedor 
                    :event="eventSelect"
                    @eliminar="eliminarContenedor(eventSelect)"></propiedades-de-contenedor>
                    <propiedades-de-contenedor 
                    :event="eventSubContenedorSelect"
                    @eliminar="eliminarContenedor(eventSubContenedorSelect)"></propiedades-de-contenedor>
                    <propiedades-de-item :item="selectItem"  @eliminar="eliminarItem(indexSelected)"></propiedades-de-item>
                </div>
            </div>
        </div>
    </div>
    `
});