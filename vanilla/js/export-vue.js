class VueBaseExport extends ExportBase{
    constructor(item){
        super(item);
    }
}

class InputTextVueExport extends VueBaseExport{
    constructor(item) {
        super(item);
    }

    get code() {
        return`
        <div class="${this.item.colmd.value}">
            <label >${this.item.label.value}</label><br>
            <input type="text" v-model="model.${this.item.name.value}" ${this.codeRules}> 
        </div>`;
    }
}

class BotonGuardarVueExport extends VueBaseExport {
    constructor(item) {
        super(item);
    }

    get code() {
        return `
        <div class="col-md-12 derecha">
            <input type="submit" value="${this.item.label.value}" style="width: auto">   
        </div>`; 
    }
}

class TitleVueExport extends VueBaseExport{
    
    constructor(item) {
        super(item);
    }

    get code() {
        return `
        <div class="col-md-12">
            <h2>${this.item.label.value}<h2> 
        </div>`;
    }
}

class TextoEstaticoVueExport extends VueBaseExport {
    constructor(item) {
        super(item);
    }

    get code() {
        const lineas = this.item.texto.value.split('\n');
        let code = '';
        for (const linea of lineas) {
            code += '<p>'+linea+'</p>';
        }
        return `
        <div class="${this.item.colmd.value}">
            ${code}
        </div>`; 
    }
}

class PanelVueExport{
    constructor(form) {
        this.form = form;
    }

    getTrasforms() {
        let transforms = [];
        for (const item of this.form.items) {
            // if (item instanceof EspacioModel) { transforms.push(new EspacioVanilla(item));}
            if (item instanceof TextoModel) { transforms.push(new InputTextVueExport(item));}
            // if (item instanceof TextareaModel) { transforms.push(new TextareaVanilla(item));}
            // if (item instanceof NumeroModel) { transforms.push(new NumeroVanilla(item));}
            // if (item instanceof FechaModel) { transforms.push(new FechaVanilla(item));}
            // if (item instanceof HoraModel) { transforms.push(new HoraVanilla(item));}
            // if (item instanceof CheckboxModel) { transforms.push(new CheckboxVanilla(item));}
            // if (item instanceof EmailModel) { transforms.push(new EmailVanilla(item));}
            // if (item instanceof LabelModel) { transforms.push(new LabelVanilla(item));}
            // if (item instanceof ImagenModel) { transforms.push(new ImagenVanilla(item));}
            if (item instanceof TitleModel) { transforms.push(new TitleVueExport(item));}
            if (item instanceof BotonGuardarModel) { transforms.push(new BotonGuardarVueExport(item));}
            if (item instanceof TextoEstaticoModel) {transforms.push(new TextoEstaticoVueExport(item));}
            // if (item instanceof LineaModel) {transforms.push(new LineaVanilla(item));}
            // if (item instanceof ImagenEstaticaModel) {transforms.push(new ImagenEstaticaVanilla(item));}
        }
        return transforms;
    }

    get code() {
        let code = '<div class="row">';
        const transforms = this.getTrasforms();
        for (const item of transforms) {
            code += item.code;
        }
        code += '</div>';
        return code;
    }
}

class FormVueExport{
    constructor(form) {
        this.form = form;
    }

    /**
     * Obtiene los trasfomadores 
     * de los contenedores del formulario
     */
    getTrasforms() {
        let transforms = [];
        for (const item of this.form.items) {
            if (item instanceof PanelModel) { transforms.push(new PanelVueExport(item));}
        }
        return transforms;
    }
    /**
     * Obtiene el modelo del contenedor
     * @param {*} contenedor 
     * @param {*} data 
     */
    getModelFromContenedor(contenedor, model) {
        for (const item of contenedor.items) {
            if (item instanceof BaseInput) { 
                model[item.name.value] = null;
            }
        }
    }

    /**
     * Obtiene el modelo de datos del arreglo de contenedores.
     * @param {*} array 
     * @param {*} model 
     */
    getModelFromArrayContenoders(array, model) {
        for (const contenedor of array.items) {
            if (contenedor instanceof PanelModel || contenedor instanceof AcordeonModel) {
                this.getModelFromContenedor(contenedor, model);
            }
        }
    }
    /**
     * regresa el modelo de datos del formulario
     */
    getModelForm() {
        let model = {};
        for (const contenedor of this.form.items) {
            if (contenedor instanceof PanelModel || contenedor instanceof AcordeonModel) {
                this.getModelFromContenedor(contenedor, model);
            }
        }
        return model;
    }

    getCode()
    {
        let code = `<div>\n<form @submit="submit($event)">`; 
        const transforms = this.getTrasforms();
        for (const item of transforms) {
            code += item.code;
        }
        code += '\n</form></div>';
    return `const formulario = {
    data: function(){ 
        return {
            model:${JSON.stringify(this.getModelForm())}
        }
    },
    methods: {
        submit(event) {
            event.preventDefault();
            this.$emit('submit', JSON.parse(JSON.stringify(this.model)) );
        }
    }, 
    template: \`${code}\`,
}`;
    }

}

