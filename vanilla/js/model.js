const css  = {
    w3c: 'w3c',
    bootstrap: 'bootstrap',
    vuetify: 'vuetify',
    vanilla: 'vanilla' 
}
const enviroments = {
    css: css.w3c
}

const alineciones = [
    {label: 'Izquierda', value: 'left'},
    {label: 'Centro', value: 'center'},
    {label: 'Derecha', value: 'right'},
];

const tamanios = [
    {label: 'md1', value: 'col-md-1'},
    {label: 'md2', value: 'col-md-2'},
    {label: 'md3', value: 'col-md-3'},
    {label: 'md4', value: 'col-md-4'},
    {label: 'md5', value: 'col-md-5'},
    {label: 'md6', value: 'col-md-6'},
    {label: 'md7', value: 'col-md-7'},
    {label: 'md8', value: 'col-md-8'},
    {label: 'md9', value: 'col-md-9'},
    {label: 'md10', value: 'col-md-10'},
    {label: 'md11', value: 'col-md-11'},
    {label: 'md12', value: 'col-md-12'},
];



/**
* BaseValor
* Clase para los atributos
*/
class BaseValor{
    constructor(label, value, visible) {        
        this.label = label ? label : 'Item';
        this.value = value ? value : 'value';
        this.visible = visible!=undefined ? visible : true;
    }
}
/**
* Base para los atributos multi valor
*/
class BaseValorOptions{
    constructor(label, value, options, multipleSelection, visible) {        
        this.label = label ? label : 'Item';
        this.value = value ? value : 'value';
        this.options = options ? options : [];
        this.multipleSelection = multipleSelection ? multipleSelection : false;
        this.visible = visible!=undefined ? visible : true;
    }
}
/**
* Base para los items del formulario
*/
class BaseItem {
    constructor(type, label, alineacion, colmd, visible, visibleRulesJs, clases) {
        this.type = type ? type : new BaseValor('Tipo', 'title', false);
        this.label = label ? label : new BaseValor('Label', 'label');
        this.alineacion = alineacion ? alineacion : new BaseValorOptions('Alineación', '', alineciones);
        this.colmd = colmd ? colmd :  new BaseValorOptions('Tamaño', 'col-md-4', tamanios);
        this.visible = visible ? visible : new BaseValorOptions('Visible', 'true', visible);
        this.visibleRulesJs = visibleRulesJs ? visibleRulesJs : new BaseValor('Reglas de visibilidad js', '');
        this.clases = clases ? clases :  new BaseValor('Clases extra', '');
    }
}
/**
* Base para los items que son inputs
*/
class BaseInput extends BaseItem{
    constructor(label, name){
        super();
        this.required =  new BaseValor('Requerido', false, false);
        this.regex = new BaseValor('Regex', '', false);
        this.name = new BaseValor('Name', 'input');
        this.name.value = name ? name : this.name.value;
        this.label.value = label ? label : this.label.value;
        this.rules = new Rules();
    }

    addRule(rule) {
        this.rules.push(rule);
    }
}

class TextoModel extends BaseInput {
    constructor(label, name) {
        super(label, name);
        this.type.value = 'texto';
    }
}

class TextareaModel extends BaseInput {
    constructor(label, name) {
        super(label, name);
        this.type.value = 'textarea';
        this.colmd.value = 'col-md-12'
    }
}

class NumeroModel extends BaseInput {
    constructor(label, name) {
        super(label, name);
        this.type.value = 'numero';
    }
}

class HoraModel extends BaseInput {
    constructor(label, name) {
        super(label, name);
        this.type.value = 'hora';
    }
}

class FechaModel extends BaseInput {
    constructor(label, name) {
        super(label, name);
        this.type.value = 'fecha';
    }
}

class EmailModel extends BaseInput {
    constructor(label, name) {
        super(label, name);
        this.type.value = 'email';
    }
}

class CheckboxModel extends BaseInput {
    constructor(label, name) {
        super(label, name);
        this.type.value = 'checkbox';
    }
}
/**
* Base para los labels
*/
class EspacioModel extends BaseItem {
    constructor() {
        super();
        this.label.value = 'Espacio';
        this.colmd.value = 'col-md-12';
        this.type.value = 'espacio';
    }
}

class LabelModel extends BaseItem{
    constructor(label){
        super();
        this.type.value = 'label';
        this.label.value = label? label: this.label.value;
        this.colmd.value = 'col-md-12';
    }
}

class LineaModel extends BaseItem {
    constructor(color){
        super();
        this.type.value = 'linea';
        this.label.value = 'Linea';
        this.color = new BaseValor('Color', 'black');
        this.color.value = color;
        this.colmd.value = 'col-md-12';
    }
}
/**
* #ITEM imagen
*/
class ImagenModel extends BaseInput{
    constructor(label, name){
        super(label, name);
        this.urlImagen = '';
        this.imagenBase64 = '';
        this.type.value = 'image';
    }

    addImagen(uri) {


    }
}
/**
* Item subtitle
*/
class TitleModel extends BaseItem{
    constructor(label){
        super();
        this.clases.visible = true;
        this.type.value = 'title';
        this.colmd.visible = false;
        this.colmd.value = 'col-md-12';
        this.label.value = label? label: this.label.value;
    }
}

class BotonGuardarModel extends BaseItem {
    constructor(label) {
        super();
        this.label.value = label;
        this.type.value = 'boton-guardar';
        this.colmd.value = 'col-md-12';
    }
}

class TextoEstaticoModel extends BaseItem {
    constructor(texto) {
        super();
        this.label.value = 'Texto estático';
        this.texto = new BaseValor('Texto', texto);
        this.type.value = 'texto-estatico';
    }

    get code() {
        const lineas = this.texto.value.split('\n');
        let code = '';
        for (const linea of lineas) {
            code += '<p>'+linea+'</p>';
        }
        return code; 
    }
}

class ImagenEstaticaModel extends BaseItem {
    constructor() {
        super();
        this.label.value = 'Imagen estática';
        this.imagen = new BaseValor('Imagen', ' ');
        this.type.value = 'imagen-estatica';
        this.ancho = new BaseValor('Ancho', '100%');
        this.alto = new BaseValor('Alto', '100%');
        this.alineacion = new BaseValorOptions('Alineación', 'left', alineciones);
        this.setImagen('./img/sin-imagen.png');
    }

    setImagen(url) {
        const obj = this;
        fetch(url).then( response => response.blob())
        .then(blob => {
            blobToBase64(blob).then(base64 => {
                obj.imagen.value = base64; 
            });
        });
    }
}

class ContenedorBase{
    constructor(id) {
        this.label = new BaseValor('Label', 'contenedor');
        this.classname = 'ContenedorBase';
        this.items = [];
        this.id = id;
    }
}

class PanelModel extends ContenedorBase {
    constructor(id) {
        super(id);
        this.label.value = 'Panel';
        this.grupo = 'contenedores';
        this.classname = 'Panel';
    }
}

class AcordeonModel extends ContenedorBase {
    constructor(id) {
        super(id);
        this.label.value = 'Acordeon';
        this.classname = 'Acordeon';
        this.grupo = 'contenedores';
    }
}

class TabsModel extends ContenedorBase {
    constructor(id) {
        super(id);
        this.label.value = 'Tabs';
        this.classname = 'Tabs';
        this.grupo = 'contenedores';
        this.items = [];
        this.addContenedor(new PanelModel('Panel'+Date.now()));
    }

    addContenedor(contenedor) {
        this.items.push(contenedor);
    }
}

/**
 * Es un arreglo de contenedores de items
 */
class FormularioModel{
    constructor() {
        this.classname = 'Formulario';
        this.items = [];
    }
}

class EventSelectTabContenedor {
    constructor(contenedor, index, padre) {
        this.contenedor = contenedor;
        this.index = index;
        this.padre = padre;
    }
}

class EventSelectContenedor {
    constructor(contenedor, index, padre) {
        this.contenedor = contenedor;
        this.index = index;
        this.padre = padre;
    }
}