function downloadFile(data,filename, type){
    var blob = new Blob([data], {type: type});
    var elem = window.document.createElement('a');
    elem.href = window.URL.createObjectURL(blob);
    elem.download = filename;        
    document.body.appendChild(elem);
    elem.click();        
    document.body.removeChild(elem);
}
class ErrorBlobToBase64 extends Error {
    constructor(mesaje) {
        super(mesaje);
    }
}
function blobToBase64(blob){
    return new  Promise( (resolve,reject) => {
        if (!(blob instanceof Blob)) {
            reject(new ErrorBlobToBase64('No es un blob el dato introducido'));
        }
        let reader = new FileReader();
        reader.onload = function (data) {
            resolve(data.target.result);
        }
        reader.readAsDataURL(blob);
    });
}
function getFileText(url) {
    return fetch(url).then(response => response.text());
}

class Foliadora {
    constructor(timestam) {
        this.cont = timestam ? Date.now() : 0;
    }

    get next() {
        this.cont ++;
        return this.cont;
    }
}

function downloadAnyBlob(blob,filename){
    let elem = window.document.createElement('a');
    elem.href = window.URL.createObjectURL(blob);
    elem.download = filename;        
    document.body.appendChild(elem);
    elem.click();        
    document.body.removeChild(elem);
}