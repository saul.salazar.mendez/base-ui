class Rule {
    constructor() {
        
    }
    get codeHtml() {
        return '';
    }
    test() {
        return true;
    }
}

class RequeridoRule extends Rule {
    constructor() {
        super();
    }
    get codeHtml() {
        return `required`;
    }
}

class NumeroRule extends Rule {
    constructor() {
        super();
    }
    get codeHtml() {
        return `pattern='\\d+'`
    }
}

class TextoRule extends Rule {
    constructor() {
        super();
    }
    get codeHtml() {
        return `pattern='\\w+'`
    }
}

class MinLenRule extends Rule {
    constructor(n) {
        super();
        this.n = n;
    }
    get codeHtml() {
        return `minlength="${this.n}"`;
    }
}

class MaxLenRule extends Rule {
    constructor(n) {
        super();
        this.n = n;
    }
    get codeHtml() {
        return `maxlength="${this.n}"`;
    }
}

class CustomRegexRule extends Rule {
    constructor(reg, messageError) {
        super();
        this.reg = reg;
        this.messageError = messageError;
    }
    get codeHtml() {
        return `pattern="${this.reg}" title="${this.messageError}"`;
    }
}


class Rules extends Array {
    constructor() {
        super();
    }

    push(rule) {
        if (rule instanceof Rule) {
            return super.push(rule);
        } else {
            return 0;
        }
    }

    test() {
        for( rule of this) {
            if (!rule.test()) {return false;}
        }
        return true;
    }
}