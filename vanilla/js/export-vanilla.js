class Vanilla extends ExportBase{
    constructor(item) {
        super(item);
    }
}
/**
*/
class TextoVanilla extends Vanilla{
    constructor(item) {
        super(item);
    }

    get code() {
        return `
        <div class="${this.item.colmd.value}">
            <label >${this.item.label.value}</label><br>
            <input type="text" name="${this.item.name.value}" ${this.codeRules}> 
        </div>`;
    }
}

/**
*/
class TextareaVanilla extends Vanilla{
    
    constructor(item) {
        super(item);
    }

    get code() {
        return `
        <div class="col-12">
            <label >${this.item.label.value}</label><br>
            <textarea style="width: 100%" name="${this.item.name.value}" ${this.codeRules}></textarea>
        </div>`;
    }
}

/**
*/
class EmailVanilla extends Vanilla{
    
    constructor(item) {
        super(item);
    }

    get code() {
        return `
        <div class="${this.item.colmd.value}">
            <label >${this.item.label.value}</label><br>
            <input type="email" name="${this.item.name.value}" ${this.codeRules}> 
        </div>`;
    }
}


/**
*/
class FechaVanilla extends Vanilla{
    
    constructor(item) {
        super(item);
    }

    get code() {
        return `
        <div class="${this.item.colmd.value}">
            <label >${this.item.label.value}</label><br>
            <input type="date" name="${this.item.name.value}" ${this.codeRules}> 
        </div>`;
    }
}

/**
*/
class HoraVanilla extends Vanilla{
    
    constructor(item) {
        super(item);
    }

    get code() {
        return `
        <div class="${this.item.colmd.value}">
            <label >${this.item.label.value}</label><br>
            <input type="time" name="${this.item.name.value}" ${this.codeRules}> 
        </div>`;
    }
}

/**
*/
class NumeroVanilla extends Vanilla{
    
    constructor(item) {
        super(item)
    }

    get code() {
        return `
        <div class="${this.item.colmd.value}">
            <label >${this.item.label.value}</label><br>
            <input type="number" name="${this.item.name.value}" ${this.codeRules}> 
        </div>`;
    }
}

class CheckboxVanilla extends Vanilla{
    
    constructor(item) {
        super(item)
    }

    get code() {
        return `
        <div class="${this.item.colmd.value}">
            <input type="checkbox" name="${this.item.name.value}" ${this.codeRules}><label >${this.item.label.value}</label> 
        </div>`;
    }
}

/**
*/
class EspacioVanilla extends Vanilla{
    
    constructor(item) {
        super(item);
    }

    get code() {
        return `
        <div class="${this.item.colmd.value}">
        </div>`;
    }
}

class LabelVanilla extends Vanilla{
    
    constructor(item) {
        super(item);
    }

    get code() {
        return `
        <div class="${this.item.colmd.value}">
            <h3>${this.item.label.value}</h3>
        </div>`;
    }
}

/**
*/
class ImagenVanilla extends Vanilla{
    
    constructor(item) {
        super(item);
    }

    get code() {
        return `
        <div class="${this.item.colmd.value}">
            <label >${this.item.label.value}</label><br>
            <input type="file" name="${this.item.name.value}"> 
        </div>`;
    }
}

/**
*/
class TitleVanilla extends Vanilla{
    
    constructor(item) {
        super(item);
    }

    get code() {
        return `
        <div class="col-md-12">
            <h2>${this.item.label.value}<h2> 
        </div>`;
    }
}

class BotonGuardarVanilla extends Vanilla {
    constructor(item) {
        super(item);
    }

    get code() {
        return `
        <div class="col-md-12 derecha">
            <input type="submit" value="${this.item.label.value}" style="width: auto">   
        </div>`; 
    }
}

class TextoEstaticoVanilla extends Vanilla {
    constructor(item) {
        super(item);
    }

    get code() {
        const lineas = this.item.texto.value.split('\n');
        let code = '';
        for (const linea of lineas) {
            code += '<p>'+linea+'</p>';
        }
        return `
        <div class="${this.item.colmd.value}">
            ${code}
        </div>`; 
    }
}

class ImagenEstaticaVanilla extends Vanilla {
    constructor(item) {
        super(item);
    }

    get code() {
        return `
        <div 
        class="${this.item.colmd.value}"
        style="text-align: ${this.item.alineacion.value};">
            <img 
            src="${this.item.imagen.value}" 
            style="width: ${this.item.ancho.value}; height: ${this.item.alto.value};">
        </div>`;
    }
}

class LineaVanilla extends Vanilla {
    constructor(item) {
        super(item);
    }

    get code() {
        return `
        <div 
        class="${this.item.colmd.value}">
            <hr style="border-color: ${this.item.color.value}"/>
        </div>`;
    }
}

class PanelVanilla {
    constructor(form) {
        this.form = form;
    }

    getTrasforms() {
        let transforms = [];
        for (const item of this.form.items) {
            if (item instanceof EspacioModel) { transforms.push(new EspacioVanilla(item));}
            if (item instanceof TextoModel) { transforms.push(new TextoVanilla(item));}
            if (item instanceof TextareaModel) { transforms.push(new TextareaVanilla(item));}
            if (item instanceof NumeroModel) { transforms.push(new NumeroVanilla(item));}
            if (item instanceof FechaModel) { transforms.push(new FechaVanilla(item));}
            if (item instanceof HoraModel) { transforms.push(new HoraVanilla(item));}
            if (item instanceof CheckboxModel) { transforms.push(new CheckboxVanilla(item));}
            if (item instanceof EmailModel) { transforms.push(new EmailVanilla(item));}
            if (item instanceof LabelModel) { transforms.push(new LabelVanilla(item));}
            if (item instanceof ImagenModel) { transforms.push(new ImagenVanilla(item));}
            if (item instanceof TitleModel) { transforms.push(new TitleVanilla(item));}
            if (item instanceof BotonGuardarModel) { transforms.push(new BotonGuardarVanilla(item));}
            if (item instanceof TextoEstaticoModel) {transforms.push(new TextoEstaticoVanilla(item));}
            if (item instanceof LineaModel) {transforms.push(new LineaVanilla(item));}
            if (item instanceof ImagenEstaticaModel) {transforms.push(new ImagenEstaticaVanilla(item));}
        }
        return transforms;
    }

    get code() {
        let code = '<div class="row">';
        const transforms = this.getTrasforms();
        for (const item of transforms) {
            code += item.code;
        }
        code += '</div>';
        return code;
    }
}

class AcordeonVanilla extends PanelVanilla{
    constructor(form) {
        super(form);
    }

    get code() {
        let code = `
        <div class="row">
        <div class="col-md-12">
        <button class="ancho-100" onclick="showHide('#${this.form.id}')">${this.form.label.value}</button>
        </div>
        <div>
        <div class="row oculto border-small" id="${this.form.id}">`;
        const transforms = this.getTrasforms();
        for (const item of transforms) {
            code += item.code;
        }
        code += '</div>';
        return code;
    }
}

class FormVanilla{
    constructor(form) {
        this.form = form;
    }
    /**
     * Obtiene los trasfomadores 
     * de los contenedores del formulario
     */
    getTrasforms() {
        let transforms = [];
        for (const item of this.form.items) {
            if (item instanceof PanelModel) { transforms.push(new PanelVanilla(item));}
            if (item instanceof AcordeonModel) { transforms.push(new AcordeonVanilla(item));}
        }
        return transforms;
    }
    /**
     * Obtiene el modelo del contenedor
     * @param {*} contenedor 
     * @param {*} data 
     */
    getModelFromContenedor(contenedor, model) {
        for (const item of contenedor.items) {
            if (item instanceof BaseInput) { 
                model[item.name.value] = null;
            }
        }
    }

    /**
     * Obtiene el modelo de datos del arreglo de contenedores.
     * @param {*} array 
     * @param {*} model 
     */
    getModelFromArrayContenoders(array, model) {
        for (const contenedor of array.items) {
            if (contenedor instanceof PanelModel || contenedor instanceof AcordeonModel) {
                this.getModelFromContenedor(contenedor, model);
            }
        }
    }
    /**
     * regresa el modelo de datos del formulario
     */
    getModelForm() {
        let model = {};
        for (const contenedor of this.form.items) {
            if (contenedor instanceof PanelModel || contenedor instanceof AcordeonModel) {
                this.getModelFromContenedor(contenedor, model);
            }
        }
        return model;
    }

    getHeader(){
        return `
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <style type="text/css">
		* {
    box-sizing: border-box;
}
.row::after {
    content: "";
    clear: both;
    display: table;
}

.ancho-100{
    width: 100%;
}

.border-small{
    border: solid 1px black;
}

input:invalid {
    border-bottom: tomato 4px solid;
}

input {
    border: none;
    width: 100%;
    border: none;
    border-bottom: black 2px solid;
}

input[type="submit"] {
    border: none;
    background-color: blue;
    width: auto;
    padding: 5px;
    color: white;
}

input[type="checkbox"] {
    border: none;
    width: auto;
}

.oculto {
    display: none;
}

.mostrar {
    display: block;
}

.derecha {
    text-align: right;
}

[class*="col-"] {
    float: left;
    padding: 15px;
    height: 100px;
}
/* For mobile phones: */
[class*="col-"] {
    width: 100%;
}
@media only screen and (min-width: 600px) {
    /* For tablets: */
    .col-s1 {width: 8.33%;}
    .col-s2 {width: 16.66%;}
    .col-s3 {width: 25%;}
    .col-s4 {width: 33.33%;}
    .col-s5 {width: 41.66%;}
    .col-s6 {width: 50%;}
    .col-s7 {width: 58.33%;}
    .col-s8 {width: 66.66%;}
    .col-s9 {width: 75%;}
    .col-s10 {width: 83.33%;}
    .col-s11 {width: 91.66%;}
    .col-s12 {width: 100%;}
}
@media only screen and (min-width: 800px) {
    /* For desktop: */
    .col-md-1 {width: 8.33%;}
    .col-md-2 {width: 16.66%;}
    .col-md-3 {width: 25%;}
    .col-md-4 {width: 33.33%;}
    .col-md-5 {width: 41.66%;}
    .col-md-6 {width: 50%;}
    .col-md-7 {width: 58.33%;}
    .col-md-8 {width: 66.66%;}
    .col-md-9 {width: 75%;}
    .col-md-10 {width: 83.33%;}
    .col-md-11 {width: 91.66%;}
    .col-md-12 {width: 100%;}
}

    </style>
    <script>
    function showHide(id) {
      const div = document.querySelector(id);
      if (div.className.indexOf('oculto') >= 0) {
        div.classList.remove('oculto');
      } else {
        div.classList.add('oculto');
      }
    }
    </script>
</head>
<body>
        `;
    }

    getFooter() {
        return `
    </body></html>
        `;
    }

    getCode() {
        let code = this.getHeader(); 
        code += '\n<form onsubmit="save(event)">' 
        const transforms = this.getTrasforms();
        for (const item of transforms) {
            code += item.code;
        }
        code += '\n</form>\n<script type="text/javascript">';
        code += `
        function save(e) {
            e.preventDefault();
			const form = e.target;
            let data = ${JSON.stringify(this.getModelForm())};
            for (const key in data) {
                const input = form.querySelector('input[name="'+key+'"]');
                if (input) {
                    data[key] = input.value;
                    if (input.type == 'checkbox') {data[key] = input.checked;}
                }else if (form.querySelector('textarea[name="'+key+'"]')){
                    data[key] = form.querySelector('textarea[name="'+key+'"]').value;
                }
            }
            console.log(data);
		}`;
        code += '\n</script>';
        code += '\n</div>';
        code += this.getFooter();
        return code;
    }
}
