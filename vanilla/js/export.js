class ExportBase {
    constructor(item) {
        this.item = item;
    }

    get codeRules(){
        let code = '';
        if (this.item instanceof BaseInput) {
            for (let rule of this.item.rules) {
                code += rule.codeHtml + ' ';
            }
        }
        return code;
    }

    get code() {
        return '';
    }
}