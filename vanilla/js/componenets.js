
Vue.component('search-input',{
    data:()=>{
        return {
            value: ''
        }
    },
    methods: {
        onChange(event) {
            this.$emit('change', this.value)
        }
    },
    template: `
    <input v-model="value" placeholder="Buscar" v-on:change="onChange($event)" style="color:black;"></input>`
});

Vue.component('checkbox', {
    props: ['item'],
    template: `
    <div style="width: 100%;" class="form-group">
        <input type="checkbox" :name="item.name.value"/>
        <span>{{item.label.value}}</span><br>  
    </div>`
});

Vue.component('espacio', {
    props: ['item'],
    template: `
    <div style="width: 100%; height: 50px; border: dotted black 1px;">  
    </div>`
});

Vue.component('texto-estatico', {
    props: ['item'],
    template: `
    <div style="width: 100%;  border: dotted black 1px;" v-html="item.code">
    </div>
    `
})

Vue.component('titulo',{
    props: ['item'],
    template: `<h2>{{item.label.value}}</h2>`
});

Vue.component('etiqueta',{
    props: ['item'],
    template: `<h3>{{item.label.value}}</h3>`
});
Vue.component('linea',{
    props: ['item'],
    template: `
    <hr :style="'border-color: '+item.color.value"/>
    `
});
Vue.component('boton-guardar',{
    props: ['item', 'formValid'],
    template: `
    <div class="derecha">
        <input class="btn btn-primary" type="submit" :value="this.item.label.value" style="width: auto" :disabled="!formValid">   
    </div>
    `
});

Vue.component('texto', {
    props: ['item'],
    template: `
        <div style="width: 100%;" class="form-group">
            <span>{{item.label.value}}<span><br>
            <input type="text"
                style="width: 100%;" 
                class="form-control" 
                :name="item.name.value"
                /><br>
        </div>
    `
});
Vue.component('numero',{
    props: ['item'],
    template: `
        <div style="width: 100%;" class="form-group">
            <span>{{item.label.value}}<span><br>
            <input type="number" style="width: 100%;" class="form-control" :name="item.name.value"/>
        </div>
    `
});
Vue.component('fecha',{
    props: ['item'],
    template: `
        <div style="width: 100%;" class="form-group">
            <span>{{item.label.value}}<span><br>
            <input type="date" style="width: 100%;" class="form-control" :name="item.name.value"/>
        </div>
    `
});
Vue.component('hora',{
    props: ['item'],
    template: `
        <div style="width: 100%;" class="form-group">
            <span>{{item.label.value}}<span><br>
            <input type="time" style="width: 100%;" class="form-control" :name="item.name.value"/>
        </div>
    `
});
Vue.component('correo',{
    props: ['item'],
    template: `
        <div style="width: 100%;" class="form-group">
            <span>{{item.label.value}}<span><br>
            <input type="email" style="width: 100%;" class="form-control" :name="item.name.value"/>
        </div>
    `
});
Vue.component('cajatexto',{
    props: ['item'],
    template: `
        <div style="width: 100%;" class="form-group">
            <span>{{item.label.value}}<span><br>
            <textarea type="time" style="width: 100%;" class="form-control" :name="item.name.value"></textarea>
        </div>
    `
});

Vue.component('form-item', {
    props: ['item'],
    mounted: function(){
        console.log('form-item', this.item);
    },
    template: `
    <div>
        {{item}}
    </div>
    `
});
Vue.component('imagen-estatica', {
    props: ['item'],
    template: `
    <div :style="'width: 100%; text-align:' + item.alineacion.value+';'" class="form-group">
        <img :src='item.imagen.value' 
        :style="'width: '+ item.ancho.value+'; height: '+item.alto.value+';' ">
    </div>
    `
});
Vue.component('form-constructor', {
    props: ['element', 'valid'],
    template:`
    <div :class="element.colmd.value" @click="$emit('selectItem', element)">
        <texto-estatico v-if="element.type.value=='texto-estatico'" :item="element"></texto-estatico>
        <espacio v-if="element.type.value=='espacio'" :item="element"></espacio>
        <etiqueta v-if="element.type.value=='label'" :item="element"></etiqueta>
        <titulo v-if="element.type.value=='title'" :item="element"></titulo>
        <linea v-if="element.type.value == 'linea'" :item='element'></linea>
        <texto v-if="element.type.value=='texto'" :item='element'></texto>
        <imagen-estatica v-if="element.type.value == 'imagen-estatica'" :item='element'></imagen-estatica>
        <numero v-if="element.type.value=='numero'" :item='element'></numero>
        <fecha v-if="element.type.value=='fecha'" :item='element'></fecha>
        <hora v-if="element.type.value=='hora'" :item='element'></hora>
        <correo v-if="element.type.value=='email'" :item='element'></correo>
        <cajatexto v-if="element.type.value=='textarea'" :item='element'></cajatexto>
        <checkbox v-if="element.type.value=='checkbox'" :item='element'></checkbox>
        <boton-guardar v-if="element.type.value=='boton-guardar'" :item='element' :formValid="valid"></boton-guardar>
    </div>
    `
});
Vue.component('propiedades-input', {
    props: ['item'],
    template:`
    <div class="col-md-12">
        <span>{{item.label}}</span><br>
        <input type="text" class="ancho-100 negro" v-model="item.value">
    </div>
    `
});
Vue.component('propiedades-input-textarea', {
    props: ['item'],
    template:`
    <div class="col-md-12">
        <span>{{item.label}}</span><br>
        <textarea type="text" class="ancho-100 negro" v-model="item.value"></textarea>
    </div>
    `
});
Vue.component('propiedades-input-select', {
    props: ['item'],
    template:`
    <div class="col-md-12">
        <span>{{item.label}}</span><br>
        <select class="ancho-100 negro"  v-model="item.value">
            <option v-for="opt in item.options" :value="opt.value">{{opt.label}}<option>
        </select>
    </div>
    `
});
Vue.component('propiedades-input-color', {
    props: ['item'],
    template:`
    <div class="col-md-12">
        <span>{{item.label}}</span><br>
        <input type="color" class="ancho-100" v-model="item.value">
    </div>
    `
});
Vue.component('propiedades-input-file', {
    props: ['item'],
    methods: {
        loadImagen(event) {
            const obj = this;
            if (event.target && event.target.files){
                const files = event.target.files;
                let file = null;
                files.length > 0 ? file = files[0] : '';
                blobToBase64(file).then(base64 => {obj.item.value = base64;})
                    .catch(error => console.log(error));
            }
        }
    },
    template:`
    <div class="col-md-12">
        <span>{{item.label}}</span><br>
        <input type="file" class="ancho-100" @change="loadImagen($event)">
    </div>
    `
});
Vue.component('propiedades-input-rules', {
    props: ['item'],
    data: function() {
        return {
            requerido: false,
            regex: false,
            regexType: null,
            regexCustom: null,
            regexCustomMsj: null,
            minlen: false,
            valminlen: 10,
            maxlen: false,
            valmaxlen: 100,
            minvalue: false,
            valminvalue: 1,
            maxvalue: false,
            valmaxvalue: 100
        }
    },
    mounted: function (){
        for(const rule of this.item.rules) {
            if (rule instanceof RequeridoRule) {this.requerido = true;}
            if (rule instanceof MinLenRule) {
                this.minlen = true;
                this.valminlen = rule.n;
            }
            if (rule instanceof MaxLenRule) {
                this.maxlen = true;
                this.valmaxlen = rule.n;
            }
            if (rule instanceof TextoRule) {
                this.regex = true;
                this.regexType = 'texto';
            }
            if (rule instanceof NumeroRule) {
                this.regex = true;
                this.regexType = 'numero';
            }
            if (rule instanceof CustomRegexRule) {
                this.regex = true;
                this.regexType = 'custom';
                this.regexCustom = rule.reg;
                this.regexCustomMsj = rule.messageError;
            }
        }
    },
    methods: {
        generateRules(){
            const reglas = new Rules();
            if (this.requerido){ reglas.push(new RequeridoRule()); }
            if (this.minlen){ reglas.push(new MinLenRule(this.valminlen)); }
            if (this.maxlen){ reglas.push(new MaxLenRule(this.valmaxlen)); }
            if (this.regex){
                switch (this.regexType) {
                    case 'texto':
                        reglas.push(new TextoRule());
                        break;
                    
                    case 'numero':
                        reglas.push(new NumeroRule());
                        break;
            
                    case 'custom':
                        reglas.push(new CustomRegexRule(this.regexCustom, this.regexCustomMsj));
                        break;
                
                    default:
                        break;
                } 
            }
            this.item.rules = reglas;
            console.log(this.item);
        }
    },
    template:`
    <div class="col-md-12">
        <form @change="generateRules()" @submit="$event.preventDefault()">
        <span>Reglas</span><br>
        <div class="small-space">
            <input type="checkbox" v-model="requerido" >Requerido<br>
        </div>
        <div class="small-space">
            <input type="checkbox" v-model="minlen"> Minlen 
            <input type="number" v-if="minlen" class="negro small-input input-number" v-model="valminlen"><br>
        </div>
        <div class="small-space">
            <input type="checkbox" v-model="maxlen"> Maxlen 
            <input type="number" v-if="maxlen" class="negro small-input input-number" v-model="valmaxlen"><br>
        </div>
        <div class="small-space">
            <input type="checkbox" v-model="regex"> Regex<br>
            <div v-if="regex">
                <input type="radio" v-model="regexType" class="small-space" value="texto">Solo texto<br>
                <input type="radio" v-model="regexType" class="small-space" value="numero">Solo numeros<br>
                <input type="radio" v-model="regexType" class="small-space" value="custom">Personalizado<br>
                <div v-if="regexType=='custom'">
                    <span>Regex personalizado</span><br>
                    <input type="text" v-model="regexCustom" class="small-space negro"><br>
                    <span>Mensaje personalizado</span><br>
                    <input type="text" v-model="regexCustomMsj" class="small-space negro"><br>
                </div> 
            </div>
        </div>
        </form>
    </div>  
    `
});
Vue.component('propiedades-de-item', {
    props: ['item'],
    methods: {
        elimina(){
            const obj = this;
            if (confirm('Desea eliminar este elemento?')) {
                obj.$emit('eliminar', this.item);
            }
        }
    },
    template:`
    <div v-if="item" class="margen-propiedad">
        <h4>Componente</h4>
        <div v-if="item instanceof BaseInput" class="row">
            <propiedades-input :item="item.label"></propiedades-input>
            <propiedades-input :item="item.name"></propiedades-input>
            <propiedades-input-select :item="item.colmd" v-if="!(item instanceof TextareaModel)"></propiedades-input-select>
            <propiedades-input-rules :item="item"></propiedades-input-rules>
        </div>
        <div v-if="item instanceof LineaModel">
            <propiedades-input-color :item="item.color"></propiedades-input-color>
        </div>
        <div v-if="item instanceof TextoEstaticoModel">
            <propiedades-input-select :item="item.colmd"></propiedades-input-select>
            <propiedades-input-textarea :item="item.texto"></propiedades-input-textarea>
        </div>
        <div v-if="item instanceof EspacioModel || item instanceof ImagenEstaticaModel">
            <propiedades-input-select :item="item.colmd"></propiedades-input-select>
        </div>
        <div v-if="item instanceof LabelModel || item instanceof TitleModel || item instanceof BotonGuardarModel" class="row">
            <propiedades-input :item="item.label"></propiedades-input>
        </div>
        <div v-if="item instanceof ImagenEstaticaModel" class="row">
            <propiedades-input-file :item="item.imagen"></propiedades-input-file>
            <propiedades-input :item="item.ancho"></propiedades-input>
            <propiedades-input :item="item.alto"></propiedades-input>
            <propiedades-input-select :item="item.alineacion"></propiedades-input-select>
        </div>
        <div class="row">
            <div class="col-md-6" style="margin-top: 10px;">
            <button class="btn btn-danger" @click="elimina()">eliminar</button>
            </div>
        </div>
    </div>
    `
});
Vue.component('propiedades-de-contenedor', {
    props: ['event'],
    methods: {
        elimina(){
            const obj = this;
            if (confirm('Desea eliminar este contenedor?')) {
                obj.$emit('eliminar', this.event);
            }
        }
    },
    template:`
    <div v-if="event" class="margen-propiedad">
        <h4>Contenedor</h4>
        <div class="row">
            <propiedades-input :item="event.contenedor.label"></propiedades-input>
        </div>
        <div class="row">
            <div class="col-md-6" style="margin-top: 10px;">
            <button class="btn btn-danger" @click="elimina()">eliminar</button>
            </div>
        </div>
        <hr>
    </div>
    `
});
Vue.component('panel-form', {
    props: ['contenedor', 'valid'],
    methods: {
        selectItem(data) {
            this.$emit('selectItem', data);
        }
    },
    template: `
    <div>
        <draggable v-model="contenedor.items" class="dragArea contenido row" :options="{group:'componentes'}">
            <form-constructor v-for="(item, index) in contenedor.items" :element="item" :valid="valid" @selectItem="selectItem({item: $event, index: index, contenedor: contenedor})" ></form-constructor>
        </draggable>
    </div>
    `
});

Vue.component('acordeon-form', {
    props: ['contenedor', 'valid'],
    methods: {
        selectItem(data) {
            this.$emit('selectItem', data);
        }
    },
    template: `
    <div>
        <div class="row">
            <div class="ancho-100">
                <button type="button" class="negro ancho-100" data-toggle="collapse" :data-target="'#form-acordeon-'+contenedor.id">
                    {{contenedor.label.value}}
                </button>
            </div>
        </div>
        <div :id="'form-acordeon-'+contenedor.id" class="collapse">
        <panel-form :contenedor="contenedor" :valid="valid" @selectItem="selectItem($event)"></panel-form>
        </div>
    </div>
    `
});

Vue.component('tabs-form', {
    props: ['contenedor', 'valid'],
    data: function() {
        return {
            foliadora: new Foliadora(false)
        }
    },
    methods: {
        selectItem(data) {
            this.$emit('selectItem', data);
        },
        selectContenedor(data, index) {
            this.$emit('selectContenedorTab', new EventSelectTabContenedor(data, index, this.contenedor));
        },
        selectContenedorEvent(data, index) {
            if (data && data.contenedor instanceof TabsModel){return;}
            if (data instanceof EventSelectTabContenedor) {
                this.$emit('selectContenedorTab', data);
            }else if(data instanceof EventSelectContenedor){
                this.$emit('selectContenedorTab', new EventSelectTabContenedor(data.contenedor, index, this.contenedor));
            }else {
                this.$emit('selectContenedorTab', new EventSelectTabContenedor(data, index, this.contenedor));
            }
        },
        setActive(item) {

            const lis = this.$el.querySelectorAll('li');
            for (const li of lis) {
                li.classList.remove('active');
            }
            const divs = this.$el.querySelectorAll('.tab-pane');
            for (const div of divs) {
                div.classList.remove('active');
                div.classList.remove('in');
            }

            const li = this.$el.querySelector('li#'+item);
            li.classList.add('active');

            const div = this.$el.querySelector('#tab'+item);
            div.classList.add('in');
            div.classList.add('active');
        },
        addPanel(){
            this.contenedor.items.push(new PanelModel('panel'+this.foliadora.next));
        }
    },
    template: `
    <div>
    <div class="row">
            <draggable v-model="contenedor.items" class="nav nav-tabs col-md-12" :options="{group:'contenedores'}">
            <li v-for="(item,index) in contenedor.items" 
            @click="setActive(item.id); selectContenedor(item,index); " :id="item.id">
                <a :data-toggle="'tab'+item.id">{{item.label.value}}</a>
            </li>
            <li @click="addPanel()">
                <a>+</a>
            </li>
            </draggable>
        
        <div class="tab-content col-md-12">
            <div class="tab-pane fade" v-for="(item,index) in contenedor.items" :id="'tab'+item.id" >
            <contenedor-constructor :contenedor="item" :valid="valid" 
            @selectItem="selectItem($event)" 
            @selectContenedor="selectContenedorEvent($event, index)"
            @selectContenedorTab="selectContenedorEvent($event, index)"></contenedor-constructor>
        </div>
    </div>
    </div>
    `
});

Vue.component('contenedor-constructor', {
    props: ['contenedor', 'valid', 'index', 'padre'],
    methods: {
        selectItem(data) {
            this.$emit('selectItem', data);
        },
        selectContenedor(data) {
            this.$emit('selectContenedor',
                new EventSelectContenedor(data, this.index, this.padre)
            );
        },
        selectContenedorTab(data) {
            console.log(data);
            this.$emit('selectContenedorTab', data)
        }
    },
    template: `
    <div @click="selectContenedor(contenedor)">
        <panel-form 
        v-if="contenedor instanceof PanelModel"
        :contenedor="contenedor" 
        :valid="valid" 
        @selectItem="selectItem($event)"></panel-form>
        {{contenedor.id}}
        <acordeon-form 
        v-if="contenedor instanceof AcordeonModel"
        :contenedor="contenedor" 
        :valid="valid" 
        @selectItem="selectItem($event)"></acordeon-form>

        <tabs-form
        v-if="contenedor instanceof TabsModel"
        :contenedor="contenedor" 
        :valid="valid"
        @selectItem="selectItem($event)"
        @selectContenedorTab="selectContenedorTab($event)"
        >
        </tabs-form>
    </div>
    `
});
