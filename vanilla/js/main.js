



function createVue(item)
{
  vue = `Vue.component( '${item.name}',
      {
        data: ()=>{ return ${JSON.stringify(item.data)};},
        methods: ${item.methods},
        template: \`${item.template}\`,
        created: ${item.created},
        mounted: ${item.mounted},
        destroyed: ${item.destroyed}
      });`
      
  eval(
    vue
  );
}


var vm = new Vue({
  el: "#main",
  data: function() {
    return {
      ideData: new FormularioDataIde()
    }
  }
});