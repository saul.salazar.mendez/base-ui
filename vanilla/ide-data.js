/**
 * Clase contenedora de los items que se moveran del panel de componenetes a formulario
 */
class ArregloItemsAMover {
    constructor(titulo, grupo, name) {
        this.titulo = titulo;
        this.grupo = grupo;
        this.name = name;
        this.items = [];
    }
}

class ArregloComponentes{
    constructor() {
        this.items = [];
    }
}

class IdeData{
    constructor() {
        this.arregloComponentes = new ArregloComponentes;
        this.formulario = new FormularioModel();
        this.foliadora = new Foliadora(false);
    }
    /**
     * Regresa el codigo hml  del formulario selecionado
     */
    guardaComponenteHtml() {
        console.log('Por definir el codigo para generar html');
    }

    /**
     * Regresa el codigo del componente local Vue
     */
    guardaComponenteVue() {
        console.log('Por definir el codigo para generar Vue componente');
    }

    /**
     * Genera el codigo del componente Angualr
     */

    guardaComponenteAngular() {
        console.log('Por definir el codigo para generar Angular componente');
    }

    creaCopia(item) {
        console.log('Por definir informacion para crear copia de los elementos que se mueven')
    }

    move(event, old) {
        console.log('Validacion para agregar elementos');
    }
    
}

class FormularioDataIde extends IdeData {
    constructor() {
        super();
        
        const panel = new PanelModel();
        const textoEstatico = new TextoEstaticoModel('Arrastre y suelte componentes en esta area. Este es un componente estatico y lo puede eliminar.');
        textoEstatico.colmd.value = 'col-md-12'; 
        panel.items.push(textoEstatico);
        this.formulario.items.push(panel);
        

        const contenedores = new ArregloItemsAMover('Contenedores', 'contenedores', 'contenedores');
        contenedores.items.push(new PanelModel());
        contenedores.items.push(new AcordeonModel());
        contenedores.items.push(new TabsModel());
        this.arregloComponentes.items.push(contenedores);

        const inputs = new ArregloItemsAMover('Inputs', 'componentes', 'inputs');
        inputs.items.push(new TextoModel('Input text', 'input'));
        inputs.items.push(new NumeroModel('Numero', 'numero'));
        inputs.items.push(new FechaModel('Fecha', 'fecha'));
        inputs.items.push(new HoraModel('Hora','hora'));
        inputs.items.push(new EmailModel('Email','email'));
        inputs.items.push(new TextareaModel('Textarea','textarea'));
        inputs.items.push(new CheckboxModel('Checkbox', 'checkbox'));
        this.arregloComponentes.items.push(inputs);

        const otros = new ArregloItemsAMover('Otros', 'componentes', 'otros');
        otros.items.push(new TitleModel('Titulo'));
        otros.items.push(new LabelModel('Etiqueta'));
        otros.items.push(new LineaModel('black'));
        otros.items.push(new TextoEstaticoModel(' '));
        otros.items.push(new BotonGuardarModel('Boton guardar'));
        otros.items.push(new EspacioModel());
        otros.items.push(new ImagenEstaticaModel());
        this.arregloComponentes.items.push(otros);
    }

    creaCopia(item) {
        const cont = this.foliadora.next;
        if (item instanceof TextoModel) {return new TextoModel('Input '+cont, 'input' + cont);}
        if (item instanceof NumeroModel) {return new NumeroModel('Numero '+cont, 'numero'+cont);}
        if (item instanceof FechaModel) {return new FechaModel('Fecha '+cont,'fecha'+cont);}
        if (item instanceof HoraModel) {return new HoraModel('Hora '+cont, 'hora'+cont);}
        if (item instanceof EmailModel) {return new EmailModel('Email '+cont, 'email'+cont);}
        if (item instanceof TextareaModel) {return new TextareaModel('Textarea '+cont, 'textarea'+cont);}
        if (item instanceof CheckboxModel) {return new CheckboxModel('Checkbox '+cont, 'checkbox'+cont);}
        
        if (item instanceof LineaModel) {return new LineaModel('#ff0000');}
        if (item instanceof TitleModel) {return new TitleModel('Título '+cont);}
        if (item instanceof LabelModel) {return new LabelModel('Título '+cont);}
        if (item instanceof TextoEstaticoModel) {return new TextoEstaticoModel('Texto estático');}
        if (item instanceof BotonGuardarModel) {return new BotonGuardarModel('Guardar');}
        if (item instanceof EspacioModel) {return new EspacioModel();}
        if (item instanceof ImagenEstaticaModel) {return new ImagenEstaticaModel();}

        if (item instanceof PanelModel) {return new PanelModel('panel'+cont);}
        if (item instanceof AcordeonModel) {return new AcordeonModel('acordeon'+cont);}
        if (item instanceof TabsModel) {return new TabsModel('tabs'+cont);}
    }

    move(event, old) {
        //validamos que los contenedores tabs no se encadenen
        const isTab = event.dragged.innerHTML.indexOf('Tabs')>=0;
        const contenedorIsTab = event.to.className.indexOf('nav nav-tabs')>=0;
        if (!isTab) {return true;}
        return !contenedorIsTab;
    }

    guardaComponenteHtml() {
        const formHtml = new FormVanilla(this.formulario);
        downloadFile(formHtml.getCode(), 'Formulario.html', 'html/text');
    }

    async guardaComponenteVue() {
        const formVue = new FormVueExport(this.formulario);
        let zip = new JSZip();
        const vue   = await getFileText('./template/html/vue/js/vue.min.js');
        const index = await getFileText('./template/html/vue/index.txt');
        const main  = await getFileText('./template/html/vue/main.js');
        const style = await getFileText('./template/html/vue/styles.css');
        zip.file('js/vue.min.js',vue);
        zip.file('styles.css', style);
        zip.file('main.js', main);
        zip.file('formulario.js', formVue.getCode());
        zip.file('index.html', index);
        zip.generateAsync({type:"blob"}).then(function(blob) {
            downloadAnyBlob(blob, 'proyecto.zip');
        });
    }
}